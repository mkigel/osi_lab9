module functionsFG
implicit none 

contains 

function Ffunc(X,Y) result(func)
implicit none 
real :: X, Y
real :: func
    func = X**2 + Y**2 - 1.0
end function
    
function Gfunc(X,Y) result(func)
implicit none 
real :: X, Y
real :: func
    func = X**2-Y**2
end function
    
! вывод в файл графика двумерной функции на равномерной ортогональной сетке
subroutine Grafik2D(X,Y,Func,NN,filename)       
implicit none 
real, intent (IN) :: Func(:,:)                ! процедуре не обязательно знать максимальный размер, но используемый размер нужен
real, intent (IN) :: X(:), Y(:)      
integer, intent (IN) :: NN
character(*), intent (IN) :: filename 
integer :: i,j 
open(unit=48,file=filename)
!write(48, '(A)') 'Variables = X, Y, F'                      ! не знаю, как оформить аналог для gnuplot 
!write(48, '(A,I4,A,I4)') 'zone I = ', NN, ' J = ', NN       ! но как gnuplot знает как точки соединять в сетку?
do i = 1, NN
    do j = 1, NN 
        write (48,'(F8.6,F9.6,E12.4)') X(j), Y(i), Func(i,j)
        !write (48,'(3(E12.4))') X(j), Y(i), Func(i,j)
        ! в фортране есть форматная запись, что позволяет регулировать "количество пробелов"
        ! например здесь "E14.4" - значит, что числа выводятся в экспоненциальном формате "   -0.9984E+00"
        ! всего число занимает 14 символов и имеет 4 знака после точки, получчается 3 пробела 
        ! если поставить меньше, чем 14, то и пробелов будет меньше. 
        ! если поствить "F" вместо "E", то будт запись с фиксированной точкой, посмотрите, что получится. 
        ! 3 - в "3(E14.4)" значит,что таких чисел будет 3 в строе
    end do
end do
close(48)
end subroutine 

end module 
