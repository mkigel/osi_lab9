
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <wait.h>


int main(int argc, char ** argv)
{
    int status, error;
    char *args;

    pid_t fork_child = fork();

    if(argc < 2)
    {
        perror("You dont put arguments");
        return EXIT_FAILURE;
    }

    if(fork_child == -1)
    {
        perror("Can't create child");
        return EXIT_FAILURE;
    }

    if(fork_child == 0)
    {
        args = argv[0];
        argv[0] = "cat";
        error = execvp("cat",argv);
        if(error == -1)
        {
            argv[0] = args;
            perror("Execvp dont work");
            return EXIT_FAILURE;
        }
    }
    pid_t wait_child;
    wait_child = wait(&status);
    if(wait_child == -1)
    {
        perror("Wait ended with error");
        return EXIT_FAILURE;
    }
    printf("\nParent: my child status is %d\n", WEXITSTATUS(status));
    return EXIT_SUCCESS;
}
